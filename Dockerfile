FROM python:3.8-alpine
ARG TOKEN
ENV TOKEN $TOKEN
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
CMD ["python3", "start.py"]
